#!/bin/bash
cd /var/www/html
ls -la
#file permissions

#clear cache and another stuff
#chmod -R 777 var/log
#chmod -R 777 var/cache
php7.4 bin/console --env=dev cache:clear
php7.4 bin/console --env=dev cache:warmup
php7.4 bin/console --env=prod cache:clear
php7.4 bin/console --env=prod cache:warmup

#setting writing rights for web server
#HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\ -f1)
#if [ "$HTTPDUSER" == "" ]; then
# HTTPDUSER="www-data"
#fi

#setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var/log var/cache var/uploads
#setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var/log var/cache var/uploads



### YARN
#yarn install
#yarn encore prod
##ASSETS
php7.4 bin/console assets:install
##MIGRATIONS
php7.4 bin/console --env=prod doctrine:migrations:migrate -q

#starting Forego to keep docker running
forego start